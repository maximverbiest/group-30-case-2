program project1;

{$mode objfpc}{$H+}

uses
    Classes, SysUtils, KUMatrixGeneric;
var
    //Prefer matrix for farm -> preferF[i,j] = 1 if Farm i prefers Shop j
    preferF,
    //Prefer matrix for shop -> preferS[i,j] = 1 if Shop j prefers farm i
    preferS,
    //Assignment matrix
    assignmentMx,
    //Defaultprofit matrix for shop
    S,
    //Defaultprofit matrix for farms
    F : TKUIntegerMatrix;

    //transferF[i,j] = transfer from farm i to shop j
    transferF,
    //transferS[i,j] = transfer from shop j to farm i
    transferS: TKURealMatrix;

    //currentShop[i] = shop assigned to farm i
    currentShop: Array[1..120] of Integer;

    //currentProfitF[i] = current profit of farm i
    currentProfitF,
    //currentProfitS[i] = current profit of shop i
    currentProfitS : Array[1..120] of Real;


//Load assignment-, transferF- and transferS matrix from CPLEX output and default profits
procedure initMatrix();
begin

  assignmentMx := TKUIntegerMatrix.Create('assignments_mx.txt');
  transferF := TKURealMatrix.Create('transferF_mx.txt');
  transferS := TKURealMatrix.Create('transferS_mx.txt');
  F := TKUIntegerMatrix.Create('F.txt');
  S := TKUIntegerMatrix.Create('S.txt');

end;

// Initializes currentProfitF[]
procedure loadProfitF();
var farmFile: TextFile;
    profitF: Real;
    whiteSpace: char;
    i: Integer;
begin
     AssignFile(farmFile,'profitF.txt');
     Reset(farmFile);
     i := 1;
     while not Eof(farmFile) do
     begin
          while not Eoln(farmFile) do
          begin
             read(farmFile,profitF,whiteSpace);
             currentProfitF[i] := profitF;
             i := i + 1
          end;
     end;
end;

// Initializes currentProfitS[]
procedure loadProfitS();
var shopFile: TextFile;
    profitS: Real;
    whiteSpace: char;
    i: Integer;
begin
     AssignFile(shopFile,'profitS.txt');
     Reset(shopFile);
     i := 1;
     while not Eof(shopFile) do
     begin
          while not Eoln(shopFile) do
          begin
             read(shopFile,profitS,whiteSpace);
             currentProfitS[i] := profitS;
             i := i + 1
          end;
     end;
end;

// Initializes currentShop[]
procedure initArrays();
var       i,j: Integer;
begin
   for i := 1 to 120 do
       begin
        //First init at 0 to make sure
        currentShop[i] := 0;
        for j := 1 to 120 do
            begin
             if assignmentMx[i,j] = 1 then
                begin
                     currentShop[i] := j;
                end;
            end;
       end;
end;

//Calculates the preferences of the shops
function computePreferenceS (): TKUIntegerMatrix;
var
   i,j: Integer;
   resultMX : TKUIntegerMatrix;
begin
     resultMX := TKUIntegerMatrix.Create(120,120,0);
     for i:=1 to 120 do
         for j:=1 to 120 do
             begin
                  if S[i,j] > currentProfitS[j] then
                  begin
                    resultMX[i,j] := 1
                  //overkill but just to make sure
                  end else resultMX[i,j] := 0;
             end;
     result := resultMX;
end;

//Calculates the preferences of the farms
function computePreferenceF():TKUIntegerMatrix;
var i,j : Integer;
    resultMx : TKUIntegerMatrix;
begin
     resultMx := TKUIntegerMatrix.Create(120,120,0);
     for i := 1 to 120 do
         for j := 1 to 120 do
             begin
                  if F[i,j] > currentProfitF[i] then
                  begin
                  resultMx[i,j] := 1;
                  end else resultMx[i,j] := 0 ;
             end;
     result := resultMx;
end;

//Calculates the sum of the 2 preference matrices
function sumPrefMatrix(): TKUIntegerMatrix;
var i,j : Integer;
    resultMx : TKUIntegerMatrix;
begin
  resultMx := TKUIntegerMatrix.Create(120,120);
  for i := 1 to 120 do
      for j := 1 to 120 do
          begin
           resultMx[i,j] := preferF[i,j] + preferS[i,j];
          end;
  result := resultMx;
end;

//Checks the stability of the assignment by evaluating the preference matrices
function checkStability(): Boolean;
var i,j,rowCounter: Integer;
    sumMx : TKUIntegerMatrix;
begin
     result := true;
     preferF := computePreferenceF();
     preferS := computePreferenceS();
     sumMx := sumPrefMatrix();
     for i := 1 to 120 do
         begin
              //Reset rowcounter every for every row
              rowCounter := 0;
              for j := 1 to 120 do
                  begin
                       if sumMx[i,j] = 2 then
                       rowCounter:= rowCounter + 1;

                       if rowCounter >= 1 then
                          begin
                               //This means there are more than one '2' in the sum matrix,
                               //implying instability
                               exit(false);
                          end;
                  end;
         end;
end;

//Checks if a transfer doesn't exceed the max value to transfer
function transferViolated(): Boolean;
var i: Integer;
begin
     result := false;

     // Check transferF
     for i := 1 to 120 do
         begin
              if transferF[i,currentShop[i]] >= F[i,currentShop[i]] then
                 exit(true);
         end;
     // Check transferS
     for i := 1 to 120 do
         begin
              if transferS[i,currentShop[i]] >= S[i,currentShop[i]] then
                 exit(true);
         end;
end;

// Checks if profits are strictly positive
function profitViolated(): Boolean;
var i: Integer;
begin
     result := false;
     for i := 1 to 120 do
         begin
              if (currentProfitF[i] <= 0) OR (currentProfitS[i] <= 0) then
                 exit(true);
         end;
end;

begin
     //Initialization
     initMatrix();
     initArrays();
     loadProfitF();
     loadProfitS();

     //Stability check
     WriteLn('Stable: ' , checkStability());

     //Transfer check
     Writeln('Transfers violated: ' , transferViolated());

     //Profit check
     WriteLn('Profits violated: ' , profitViolated());



     ReadLn();
end.

