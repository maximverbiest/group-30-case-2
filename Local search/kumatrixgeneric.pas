unit KUMatrixGeneric;
{$MODE OBJFPC}

{ Author: Seppe vanden Broucke }
{ Version: 2014-09-23 }

{ ============================================================================ }
{ ============================= INTERFACE ==================================== }
{ ============================================================================ }

interface

uses
  SysUtils, Classes, typinfo;

type
  generic TKUMatrix<_T> = class(TObject)
    private
    Items: array of _T;
    Rows: Integer;
    Cols: Integer;
    procedure ShowError(message: String; filename: String; called: Integer; matrix: TKUMatrix; throwexception: Boolean);
    function Split(stringtosplit: String; delimiter: Char): TStringList;
    function GenericFormat(value: _T): String;
    procedure ZeroMemory(Destination: Pointer; Length: DWORD);
    public
    constructor Create(nrrows: Integer; nrcols: Integer); overload;
    constructor Create(nrrows: Integer; nrcols: Integer; value: _T); overload;
    constructor Create(filename: String); overload;
    constructor Create(nrrowsorcols : Integer); overload;
    function IsEqual(othermatrix: TKUMatrix): Boolean;
    function  Copy(): TKUMatrix;
    function  GetCell(row: Integer; col: Integer): _T;
    procedure SetCell(row: Integer; col: Integer; value: _T);
    function  GetCell(roworcol: Integer): _T;
    procedure SetCell(roworcol: Integer; value: _T);
    function  NrCols(): Integer;
    function  NrRows(): Integer;
    procedure SwapRows(row1, row2: Integer);
    procedure SwapCols(col1, col2: Integer);
    procedure SwapCells(row1, col1, row2, col2: Integer);
    procedure SwapCells(roworcol1, roworcol2: Integer);
    procedure Save(FileName: String);
    function  ToStr(): String;
    function  ToShortStr(): String;
    procedure Show();
    procedure ShowShort();
    property Cell[row: Integer; col: Integer] : _T read GetCell write SetCell; default;
    property Element[roworcol: Integer] : _T read GetCell write SetCell;
  end;
  TKUIntegerMatrix = specialize TKUMatrix<Integer>;
  TKURealMatrix = specialize TKUMatrix<Real>;
  TKUStringMatrixNonDelimited = specialize TKUMatrix<AnsiString>;
  TKUStringMatrix = class(TKUStringMatrixNonDelimited)
  public
    constructor Create(filename: String); overload;
    constructor Create(filename: String; delimiter: Char); overload;
  end;
  TKUCharMatrix = specialize TKUMatrix<Char>;

{ ============================================================================ }
{ ============================= IMPLEMENTATION =============================== }
{ ============================================================================ }

implementation

procedure TKUMatrix.ShowError(message: String; filename: String; called: Integer; matrix: TKUMatrix; throwexception: Boolean);
begin
     writeln;
     writeln;
     writeln;
     writeln('   .   EEN FOUT TRAD OP BIJ HET MANIPULEREN VAN EEN MATRIX:');
     writeln('  / \  "'+message+'"');
     write  (' / ! \ ');
     if filename <> '' then
        writeln('Controleer het bestaan van volgend bestand: '+filename)
     else if matrix <> nil then
        writeln('U riep positie '+IntToStr(called)+' op, matrix heeft dimensie ('+IntToStr(matrix.nrrows)+','+IntToStr(matrix.nrcols)+')');
     writeln(' ----- Controleer uw oproep (matrix posities beginnen vanaf 1)');
     if throwexception then begin
        writeln;
        writeln('*** Na het drukken op ENTER zal er een Exception worden opgeroepen. ***');
        writeln;
        readln;
        raise Exception.Create(message);
     end;
end;

function TKUMatrix.Split(stringtosplit: String; delimiter: Char): TStringList;
var
  outputList: TStringList;
begin
  outputList := TStringList.Create;
  outputList.Clear;
  outputList.StrictDelimiter := True;
  outputList.Delimiter := delimiter;
  outputList.DelimitedText := stringtosplit;
  result := outputList;
end;

function TKUMatrix.GenericFormat(value: _T): String;
{ Magic formatting function with pointer hocus-pocus. Only tested with Real,
  Integer, Char and String. }
var
  text: String;
begin
  text := '';
  if (TypeInfo(_T) = TypeInfo(String)) or (TypeInfo(_T) = TypeInfo(AnsiString)) then
     result := PString(@value)^
  else if TypeInfo(_T) = TypeInfo(Integer) then
  begin
     Str(PInteger(@value)^:5, text);
     result := text
  end
  else if TypeInfo(_T) = TypeInfo(Real) then
  begin
     Str(PDouble(@value)^:5:2, text);
     result := text
  end
  else if TypeInfo(_T) = TypeInfo(Char) then
     result := PChar(@value)^
  else result := '?';
end;

procedure TKUMatrix.ZeroMemory(Destination: Pointer; Length: DWORD);
{ Magic fillchar function wrapper, avoids compiler hinting. Replaces the
  Default() function in Delphi. }
begin
  FillChar(Destination^, Length, 0);
end;

constructor TKUMatrix.Create(filename: String);
var
  DataFile: TextFile;
  len: Integer;
  seencols: Integer;
  item: _T;
begin
     if not FileExists(filename) then
        ShowError('TKUMatrix.Create: Matrix bestand bestaat niet', filename, 0, nil, true);

     self.Create(0, 0);
     ZeroMemory(@item, SizeOf(item));

     len := 0;
     seencols := 0;
     SetLength(self.Items, len);
     AssignFile(DataFile, FileName);
     Reset(DataFile);

     while not EOF(DataFile) do
     begin
          seencols := 0;
          self.Rows := self.Rows + 1;
          while not EOLN(DataFile) do
          begin
               read(DataFile, item);
               seencols := seencols + 1;
               len := len + 1;
               SetLength(self.Items, len);
               self.Items[len-1] := item;
          end;
          if self.Cols = 0 then
             self.Cols := seencols
          else if self.Cols <> seencols then
             ShowError('TKUMatrix.Create: Matrix bestand bevat ongeldige lijn', filename, 0, nil, true);
          readln(DataFile);
     end;
     CloseFile(DataFile);

     if (self.Rows = 0) or (self.Cols = 0) then
     begin
          self.Rows := 0;
          self.Cols := 0;
     end;
end;

constructor TKUStringMatrix.Create(filename: String);
begin
     self.Create(filename, #0);
end;

constructor TKUStringMatrix.Create(filename: String; delimiter: Char);
var
  DataFile: TextFile;
  len: Integer;
  seencols: Integer;
  item: _T;
  i: Integer;
  delimlist: TStringList;
begin
     if not FileExists(filename) then
        ShowError('TKUStringMatrix.Create: Matrix bestand bestaat niet', filename, 0, nil, true);

     self.Create(0, 0);
     ZeroMemory(@item, SizeOf(item));

     len := 0;
     seencols := 0;
     SetLength(self.Items, len);
     AssignFile(DataFile, FileName);
     Reset(DataFile);

     while not EOF(DataFile) do
     begin
          seencols := 0;
          self.Rows := self.Rows + 1;
          while not EOLN(DataFile) do
          begin
               read(DataFile, item);
               delimlist := Split(item, delimiter);
               for i := 0 to delimlist.Count-1 do
               begin
                    seencols := seencols + 1;
                    len := len + 1;
                    SetLength(self.Items, len);
                    self.Items[len-1] := delimlist[i];
               end;
          end;
          if self.Cols = 0 then
             self.Cols := seencols
          else if self.Cols <> seencols then
             ShowError('TKUStringMatrix.Create: Matrix bestand bevat ongeldige lijn', filename, 0, nil, true);
          readln(DataFile);
     end;
     CloseFile(DataFile);

     if (self.Rows = 0) or (self.Cols = 0) then
     begin
          self.Rows := 0;
          self.Cols := 0;
     end;
end;

constructor TKUMatrix.Create(nrrows: Integer; nrcols: Integer);
var item :_T;
begin
     ZeroMemory(@item, SizeOf(item));
     self.Create(nrrows, nrcols, item);
end;

constructor TKUMatrix.Create(nrrowsorcols : Integer);
begin
	self.Create(1,nrrowsorcols);
end;

constructor TKUMatrix.Create(nrrows: Integer; nrcols: Integer; value: _T);
var r, c: Integer;
begin
     self.Rows := nrrows;
     self.Cols := nrcols;
     SetLength(self.Items, nrrows * nrcols);
     for r := 1 to self.NrRows() do
         for c := 1 to self.NrCols() do
             self.SetCell(r, c, value);
end;

function TKUMatrix.IsEqual(othermatrix: TKUMatrix): Boolean;
var
  r, c: Integer;
begin
  { Should use Equals() override here but type equality checking is hard... }
  result := true;
  if othermatrix = nil then result := false
  else if self.nrrows <> othermatrix.nrrows then result := false
  else if self.nrcols <> othermatrix.nrcols then result := false
  else
  begin
       for r := 1 to self.NrRows() do
       begin
            for c := 1 to self.NrCols() do
            begin
                 if self.getcell(r,c) <> othermatrix.getcell(r,c) then
                 begin
                      result := false;
                      break;
                 end;
            if result = false then break;
            end;
       end;
  end;
end;

function TKUMatrix.Copy(): TKUMatrix;
var r, c: Integer;
begin
     result := TKUMatrix.Create(self.NrRows, self.NrCols);
     for r := 1 to self.NrRows() do
         for c := 1 to self.NrCols() do
             result.SetCell(r, c, self.GetCell(r, c));
end;

function  TKUMatrix.NrRows(): Integer;
begin
     result := self.Rows;
end;

function  TKUMatrix.NrCols(): Integer;
begin
     result := self.Cols;
end;

procedure TKUMatrix.SetCell(row: Integer; col: Integer; value: _T);
var
  pos: Integer;
begin
     if (row < 1) or (row > self.NrRows) then
        ShowError('TKUMatrix.SetCell: Ongeldig rijnummer', '', row, self, true);
     if (col < 1) or (col > self.NrCols) then
        ShowError('TKUMatrix.SetCell: Ongeldig kolomnummer', '', col, self, true);
     pos := (row - 1) * self.NrCols() + col - 1;
     self.Items[pos] := value;
end;

function TKUMatrix.GetCell(row: Integer; col: Integer): _T;
var
  pos: Integer;
begin
     if (row < 1) or (row > self.NrRows) then
        ShowError('TKUMatrix.GetCell: Ongeldig rijnummer', '', row, self, true);
     if (col < 1) or (col > self.NrCols) then
        ShowError('TKUMatrix.GetCell: Ongeldig kolomnummer', '', col, self, true);
     pos := (row - 1) * self.NrCols() + col - 1;
     result := self.Items[pos];
end;

procedure TKUMatrix.SetCell(roworcol: Integer; value: _T);
begin
     if (self.NrRows > 1) and (self.NrCols > 1) then
        ShowError('TKUMatrix.SetCell: Deze matrix is geen vector (geef rij en kolom mee)', '', roworcol, self, true);
     if self.NrRows = 1 then self.SetCell(1, roworcol, value)
     else self.SetCell(roworcol, 1, value);
end;

function TKUMatrix.GetCell(roworcol: Integer): _T;
begin
     if (self.NrRows > 1) and (self.NrCols > 1) then
        ShowError('TKUMatrix.SetCell: Deze matrix is geen vector (geef rij en kolom mee)', '', roworcol, self, true);
     if self.NrRows = 1 then result := self.GetCell(1, roworcol)
     else result := self.GetCell(roworcol, 1);
end;

procedure TKUMatrix.SwapRows(row1, row2: Integer);
var
   i: Integer;
begin
     for i := 1 to self.NrCols do
     begin
          self.SwapCells(row1, i, row2, i);
     end;
end;

procedure TKUMatrix.SwapCols(col1, col2: Integer);
var
   i: Integer;
begin
     for i := 1 to self.NrRows do
     begin
          self.SwapCells(i, col1, i, col2);
     end;
end;

procedure TKUMatrix.SwapCells(row1, col1, row2, col2: Integer);
var
   v: _T;
begin
     v := self.GetCell(row1, col1);
     self.SetCell(row1, col1, self.GetCell(row2, col2));
     self.SetCell(row2, col2, v);
end;

procedure TKUMatrix.SwapCells(roworcol1, roworcol2: Integer);
var
   v: _T;
begin
     v := self.GetCell(roworcol1);
     self.SetCell(roworcol1, self.GetCell(roworcol2));
     self.SetCell(roworcol2, v);
end;

procedure TKUMatrix.Save(FileName: String);
var
  DataFile: TextFile;
  r, c: Integer;
begin
     AssignFile(DataFile, FileName);
     ReWrite(DataFile);

     for r := 1 to self.NrRows() do
     begin
          for c := 1 to self.NrCols do
          begin
               write(DataFile, self.GetCell(r, c));
               if (TypeInfo(_T) <> TypeInfo(Char)) and (c <> self.NrCols) then
                  write(DataFile, #9);
          end;
          writeln(DataFile);
     end;
     CloseFile(DataFile);
end;

function TKUMatrix.ToStr(): String;
var
  r, c: Integer;
begin
     result := '';

     result := result + '     ';
     for c := 1 to self.NrCols do
          result := result + ' | ' + Format('%5d', [c]);
     result := result + #13 + #10;

     result := result + '-----';
     for c := 1 to self.NrCols do
          result := result + '-+-' + '-----';
     result := result + #13 + #10;

     for r := 1 to self.NrRows do
     begin
          result := result + Format('%5d', [r]);
          for c := 1 to self.NrCols do
          begin
               result := result + ' | ';
               if TypeInfo(_T) = TypeInfo(Char) then result := result + '    ';
               result := result + self.GenericFormat(self.GetCell(r, c));
          end;
          result := result + #13 + #10;
     end;
     result := result + #13 + #10;
end;

function TKUMatrix.ToShortStr(): String;
var
  r, c: Integer;
begin
     result := '';
     for r := 1 to self.NrRows do
     begin
          for c := 1 to self.NrCols do
          begin
               result := result + self.GenericFormat(self.GetCell(r, c));
          end;
          result := result + #13 + #10;
     end;
     result := result + #13 + #10;
end;

procedure TKUMatrix.Show();
begin
     write(self.ToStr());
end;

procedure TKUMatrix.ShowShort();
begin
     write(self.ToShortStr());
end;

end.
