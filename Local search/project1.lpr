program LocalSearch;

{$mode objfpc}{$H+}

  uses Classes, SysUtils, KUMatrixGeneric;
  var  S,F: TKUIntegerMatrix;
       shop, shopOpt : array[1..120] of Integer;
       currentProfitF, currentProfitS, tF, tS, tFOpt, tSOpt: array[1..120]
         of Real;
       sumOptGen : Real;
       counter , infeasibleCounter, m: Integer;

  {
  Returns an Int Matrix from a txt file
  }
  function fillIntMx(inputFile: String): TKUIntegerMatrix;
  begin
     result:= TKUIntegerMatrix.Create(inputFile);
  end;
  {
  Returns a Real Matrix from a txt file
  }
  function fillRealMx(InputFile: String): TKURealMatrix;
  begin
     result := TKURealMatrix.Create(InputFile);
  end;

  {
  Function that calculates the preference matrix for the shops
  Returns an Integer Mx containing the preferences
  }
  function computePrefS( currentProfitS: array of Real): TKUIntegerMatrix;
   var     row,col: Integer;
           resultMx : TKUIntegerMatrix;
   begin
        resultMx := TKUIntegerMatrix.Create(120,120);
        for row := 1 to 120 do
            for col := 1 to 120 do
                begin
                  if S[row,col] > currentProfitS[row] then
                     resultMx[row,col] := 1
                     else resultMx[row,col] := 0
                end;
        result := resultMx;
   end;

  {
  Function that calculates the preference matrix for the farms
  Returns an Integer Mx containing the preferences
  }
  function computePrefF( currentProfitF: array of Real): TKUIntegerMatrix;
   var     row,col: Integer;
           resultMx : TKUIntegerMatrix;
   begin
        resultMx := TKUIntegerMatrix.Create(120,120);
        for row := 1 to 120 do
            for col := 1 to 120 do
                begin
                  if F[row,col] > currentProfitF[row] then
                     resultMx[row,col] := 1
                     else resultMx[row,col] := 0
                end;
        result := resultMx;
   end;

  {
  Function that calculates the sum of 2 matrices
  }
  function computeSum( matrix1, matrix2 : TKUIntegerMatrix) : TKUIntegerMatrix;
  var      row,col: integer;
           resultMx : TKUIntegerMatrix;
  begin
       resultMx := TKUIntegerMatrix.Create(120,120);
       for row := 1 to 120 do
           for col := 1 to 120 do
               begin
                    resultMx[row,col] := matrix1[row,col] + matrix2[row,col];
               end;
       result := resultMx;
  end;

  {
  i : farm index
  Calculates the value that is the minimal possible profit for farm i to obtain
  a lasting assignment
  }
  function maxF( i: Integer; currentProfitS: array of Real) : Integer;
  var      y,max : Integer;
           PS: TKUIntegerMatrix;
  begin
       PS := computePrefS(currentProfitS);
       max := 0;

       for y :=1 to 120 do
           if (y <> shop[i]) and (F[i,y] >= max) and (PS[i,y] = 1)
              then
              max := F[i,y] ;

       result := max;

  end;
  {
  i : farm index
  Calculates the value that is the minimal possible profit for shop[i] to obtain
  a lasting assignment
  }
  function maxS( i: Integer; currentProfitF: array of Real) : Integer;
  var      y,max : Integer;
           PF: TKUIntegerMatrix;
  begin
       PF := computePrefF(currentProfitF);
       max := 0;

       for y :=1 to 120 do
           if (y <> shop[i]) and (S[i,y] >= max) and (PF[i,y] = 1)
              then max := S[i,y];

       result := max;

  end;

  {
  Calculates the sum of all the values in an array
  }
  function sumArray(a: array of Real) : Real;
  var      sum : Real =0;
           i : Integer;
  begin
       for i := 1 to 120 do
           sum := sum + a[i];

       result := sum;
  end;

  {
  Sets the shop array to the optimal shop array
  }
  procedure fillShops();
  var       i : Integer;
  begin
       for i := 1 to 120 do
           shop[i] := shopOpt[i];
  end;

  {
  Resets all transfer values to 0
  }
  procedure resetTransfers();
  var       i : Integer;
  begin
       for i := 1 to 120 do
           begin
             tF[i] := 0;
             tS[i] := 0;
           end;
  end;

  {
  Initialize the current profits of farms and shops to default profits
  }
  procedure InitCurrentProfits();
  var       i: Integer;
  begin
       for i := 1 to 120 do
           begin
                currentProfitF[i] := F[i,shop[i]];
                currentProfitS[i] := S[i,shop[i]];
           end;
  end;

  {
  Initializes the optimal shop array
  }
  procedure initShopOpt(x : TKUIntegerMatrix);
  var i,j : integer;
  begin
       for i := 1 to 120 do
           for j := 1 to 120 do
               begin
                 if x[i,j] = 1 then shopOpt[i] := j;
               end;
  end;

  function localSearch(startingSolution, r: Real; randomi : Integer) :Real;

  var       sumOptNbh, farmResidu, shopResidu,transferVal, sum : Real;
            randomIndex, swappedShop, c, d,e : Integer;
            infeasible : Boolean;
  begin
       sumOptGen:= 0;
       counter:=0;
       infeasibleCounter:=0;
       //Init neigbourhoodsum
       sumOptNbh := startingSolution;

       while sumOptNbh >= sumOptGen do
             begin
               counter := counter + 1;
               sumOptGen := sumOptNbh;
               fillShops();
               resetTransfers();
               //A new neighbourhood will be evaluated
               sumOptNbh := 0;
               //Pick a random farm
               randomIndex:= randomi;

               for c := 1 to 120 do
                   begin
                     if randomIndex<> c then
                        begin
                             swappedShop:= shop[randomIndex];
                             shop[randomIndex] := shop[c];
                             shop[c] := swappedShop;
                             InitCurrentProfits();
                             infeasible := false;

                             //Here the real search starts
                             for d := 1 to 120 do
                             begin

                                  farmResidu:= F[d,shop[d]] - maxF(d,currentProfitS);
                                  if farmResidu < 0 then farmResidu:= farmResidu/r;

                                  shopResidu:= maxS(d,currentProfitF) - S[d,shop[d]];
                                  if shopResidu > 0 then shopResidu:= shopResidu/r;

                                  if farmResidu < shopResidu then
                                     begin
                                          infeasible:= true;
                                     end
                                  else if (shopResidu <= 0) and (farmResidu <= 0) then
                                  begin
                                       transferVal:= farmResidu;
                                  end
                                  else if (shopResidu >= 0) and (farmResidu >= 0) then
                                  begin
                                  transferVal:= shopResidu;
                                  end
                                  //Just else would suffise?
                                  else if (shopResidu <= 0) and (farmResidu >= 0) then
                                  begin
                                   transferVal:= 0;
                                  end;

                                  if(infeasible) then
                                  begin
                                       sum := 0;
                                       infeasibleCounter:= infeasibleCounter +1;

                                       {if d > 1 then
                                       begin
                                            WriteLn('d-iterator = ' + IntToStr(d));
                                       end;}

                                       //This statement will break to line 261
                                       break;
                                  end else if transferVal >= 0 then
                                  begin

                                       currentProfitF[d] := F[d,shop[d]] - transferVal;
                                       currentProfitS[d] := S[d,shop[d]] + transferVal*r;
                                       tF[d] := transferVal;
                                  end else if transferVal < 0 then
                                  begin

                                       currentProfitF[d] := F[d,shop[d]] - transferVal*r;
                                       currentProfitS[d] := S[d,shop[d]] + transferVal;
                                       tS[d] := transferVal;
                                  end;

                             end;
                             //Here the break statement will land
                        end;
                        if not(infeasible) then sum:= sumArray(currentProfitF)+ sumArray(currentProfitS);

                        if(sum >= sumOptNbh) and not(infeasible) then
                        begin
                             for e := 1 to 120 do
                                 begin
                                   shopOpt[e] := shop[e];
                                   tFOpt[e] := tF[e];
                                   tSOpt[e] := tS[e];
                                   sumOptNbh:= sum;
                                 end;
                        end;
                        //Reset neighbourhood
                        shop[c] := shop[randomIndex];
                        shop[randomIndex] := swappedShop;
                   end;

             end;
       result := sumOptGen;
  end;

begin
  F := fillIntMx('F.txt');
  S := fillIntMx('S.txt');
  //Init the assignment of the starting solution
  initShopOpt(fillIntMx('assignment_matrix.txt'));

  //Local searches for 120 swaps
  for m := 1 to 120 do
      begin
           writeln(localSearch(39533,0.6,m));
      end;

  //writeln(sumOptGen);
  //writeln(counter);
  //writeln(infeasibleCounter);

  WriteLn('-------------------Execution finished-------------------');
  ReadLn();

end.
