program project1;

{$mode objfpc}{$H+}

uses
  Classes, SysUtils, KUMatrixGeneric, math;
type pair = array[1..2] of Integer;
var
   //Global variables
   S,F,assignMx,M : TKUIntegerMatrix;
   currentShop : array[1..120] of Integer;
   currentProfitFarm,currentProfitShop, transferF, transferS: array[1..120] of Double;
   pF,pS : TKUIntegerMatrix;
   r: Double;
   g : pair;

//Calculates the sum of 2 matrices
function sumMatrix(mx1,mx2:TKUIntegerMatrix): TKUIntegerMatrix;
var i,j : Integer;
    resultMx : TKUIntegerMatrix;
begin
  resultMx := TKUIntegerMatrix.Create(120,120);
  for i := 1 to 120 do
      for j := 1 to 120 do
          begin
           resultMx[i,j] := mx1[i,j] + mx2[i,j];
          end;
  result := resultMx;
end;

//Returns the profit of a given shop
function getProfitShop(shop: Integer): Double;
var k : Integer;
begin
  for k := 1 to 120 do
      begin
       if currentShop[k] =shop then
          begin
               result := currentProfitShop[k];
               exit;
          end;
      end;
end;

//Calculates the preferences of the shops
function computePreferenceS (): TKUIntegerMatrix;
var
   i,j: Integer;
   resultMX : TKUIntegerMatrix;
begin
     resultMX := TKUIntegerMatrix.Create(120,120,0);
     for i:=1 to 120 do
         for j:=1 to 120 do
             begin
                  if S[i,j] > getProfitShop(j) then
                  begin
                    resultMX[i,j] := 1
                  end else resultMX[i,j] := 0;
             end;
     result := resultMX;
end;

//Calculates the preferences of the farms
function computePreferenceF():TKUIntegerMatrix;
var i,j : Integer;
    resultMx : TKUIntegerMatrix;
begin
     resultMx := TKUIntegerMatrix.Create(120,120,0);
     for i := 1 to 120 do
         for j := 1 to 120 do
             begin
                  if F[i,j] > currentProfitFarm[i] then
                  begin
                  resultMx[i,j] := 1;
                  end else resultMx[i,j] := 0 ;
             end;
     result := resultMx;
end;

//Returns the farm assigned to a given shop
function findFarmForShop(shop: Integer): Integer;
var i: Integer;
begin
     for i := 1 to 120 do
         begin
              if currentShop[i] = shop then
                 Exit(i);
         end;
end;

//Finds a pair of [Farm,Shop] that want to work together
function findTwo():  pair;
var i,j : Integer;
begin
     result[1] := 999999;
     Result[2] := 999999;

     for i := 1 to 120 do
         for j := 1 to 120 do
             begin
              if (M[i,j] = 2) AND (j <> currentShop[i]) then
                 begin
                      Result[1] := i;
                      Result[2] := j;
                      exit();
                 end;
             end;
end;

//Checks if the farm wants to leave an assignment when it has a new profit due to transferring
function checkNewProblemsFarm(farmNumber, shopNumber: Integer; newProfit: Double): boolean;
var i : Integer;
begin
     result := false;
     for i := 1 to 120 do
         begin
          if(i <> shopNumber) AND (pS[farmNumber,i] = 1) AND (M[farmNumber,i] = 1) AND (F[farmNumber,i] > newProfit) then
               exit(true);
         end;
end;

//Checks if the shop wants to leave an assignment when it has a new profit due to transferring
function checkNewProblemsShop(farmNumber,shopNumber : Integer; newProfit: Double): Boolean;
var i : Integer;
begin
     result := false;
     for i := 1 to 120 do
         begin
          if (i <> farmNumber) AND (pF[i,shopNumber] = 1) AND (M[i,shopNumber] = 1) AND (S[i,shopNumber] > newProfit) then
             exit(true);
         end;
end;

//Checks if a farm can transfer to a shop and contain a lasting assignment. If it is possible, this function will also update the transfer values etc
function canTransferToShop(): boolean;
var testTransfer1: Double = 0;
    testTransfer2: Double = 0;
    a,b,c : Integer;
    newProfitFarm: Double;
begin
     result := true;
     a := g[1];
     b := currentShop[g[1]];
     c := g[2];

     if transferS[a] = 0 then
        begin
             testTransfer2:= (S[a,c] - currentProfitShop[a])/r;
             newProfitFarm:= currentProfitFarm[a] - testTransfer2;

             if(newProfitFarm > 0) AND (checkNewProblemsFarm(a,b,newProfitFarm) = false) then
                begin
                     transferF[a] := transferF[a] + testTransfer2;
                     currentProfitShop[a] := S[a,c];
                     currentProfitFarm[a] := newProfitFarm;
                end else result := false;
        end else if transferS[a] < (S[a,c] - currentProfitShop[a]) then
           begin
                testTransfer1:= transferS[a];
                testTransfer2:= (S[a,c] - currentProfitShop[a] - transferS[a])/r;
                newProfitFarm:= currentProfitFarm[a] - r*testTransfer1 - testTransfer2;

                if (newProfitFarm > 0) AND (checkNewProblemsFarm(a,b,newProfitFarm) = false) then
                   begin
                        transferF[a] := transferF[a] + testTransfer2;
                        transferS[a] := 0;
                        currentProfitFarm[a] := newProfitFarm;
                        currentProfitShop[a] := S[a,c];
                   end else result := false;
           end else
           begin
                testTransfer1:= S[a,c] - currentProfitShop[a];
                newProfitFarm:= currentProfitFarm[a] - r*testTransfer1;

                if(newProfitFarm > 0) and (checkNewProblemsFarm(a,b,newProfitFarm) = false) then
                   begin
                        transferF[a] := 0;
                        transferS[a] := transferS[a] - testTransfer1;
                        currentProfitFarm[a] := newProfitFarm;
                        currentProfitShop[a] := S[a,c];
                   end else result := false;
           end;
end;

//Checks if a shop can transfer to a shop and contain a lasting assignment. If it is possible, this function will also update the transfer values etc
function canTransferToFarm(): boolean;
var testTransfer1: Double = 0;
    testTransfer2: Double = 0;
    a,b,c : Integer;
    newProfitShop: Double;
begin

     result := true;

     a := g[1];
     b := currentShop[g[1]];
     c := g[2];

     if transferF[a] = 0 then
        begin
             testTransfer2:= (F[a,c] - currentProfitFarm[a])/r;
             newProfitShop:= currentProfitShop[a] - testTransfer2;

             if(newProfitShop > 0) AND (checkNewProblemsShop(a,b,newProfitShop) = false) then
                begin
                     transferS[a] := transferS[a] + testTransfer2;
                     currentProfitShop[a] := newProfitShop;
                     currentProfitFarm[a] := F[a,c];
                end else result := false;
        end else

     if transferF[a] < (F[a,c] - currentProfitFarm[a]) then
        begin
             testTransfer1:= transferF[a];
             testTransfer2:=((F[a,c]-currentProfitFarm[a]) - transferF[a])/r;
             newProfitShop:= currentProfitShop[a] - r*testTransfer1 - testTransfer2;

             if(newProfitShop > 0) AND (checkNewProblemsShop(a,b,newProfitShop) = false )  then
                begin
                     transferS[a] := transferS[a] + testTransfer2;
                     transferF[a] := 0;
                     currentProfitShop[a] := newProfitShop;
                     currentProfitFarm[a] := F[a,c];
                end else result := false;

        end else
        begin
         testTransfer1:= F[a,c] - currentProfitFarm[a];
         newProfitShop:= currentProfitShop[a] - r*testTransfer1;

         if(newProfitShop >0) AND(checkNewProblemsShop(a,b,newProfitShop) = false) then
            begin
                 transferS[a] := 0;
                 transferF[a] := transferF[a] - testTransfer1;
                 currentProfitShop[a] := newProfitShop;
                 currentProfitFarm[a] := F[a,c];
            end else Result := false;
        end;
end;

//Load matrices
procedure initMatrix();
begin
     assignMx := TKUIntegerMatrix.Create('assignment_mx.txt');
     F := TKUIntegerMatrix.Create('F.txt');
     S := TKUIntegerMatrix.Create('S.txt');
end;

//Initialize arrays
procedure initArrays();
var i , j , k : Integer;
begin
     //Init currentShop
     for i := 1 to 120 do
         for j := 1 to 120 do
             begin
              if assignMx[i,j] = 1 then
                 currentShop[i] := j;
             end;
     //Init transfers
     for k := 1 to 120 do
         begin
          transferF[k] := 0;
          transferS[k] := 0;
         end;
end;

//Calculates the profit
function calculateProfit(): Double;
var i : Integer;
begin
     result := 0;
     for i := 1 to 120 do
         begin
            result := result + currentProfitFarm[i] + currentProfitShop[i];
         end;
end;

//The main searching algorithm
procedure doSearch();
var i,whileCounter,swappedShop,v : Integer;
    Finished: Boolean;


begin
     //To check how much the while is entered
     whileCounter:= 0;

     //init currentProfits
     for i := 1 to 120 do
         begin
          currentProfitFarm[i] := F[i,currentShop[i]];
          currentProfitShop[i] := S[i,currentShop[i]];
         end;

     Finished:= false;

     while(Finished = false) do
     begin
          //Evaluation output
          whileCounter:= whileCounter + 1;
          if (whileCounter mod 1000) = 0 then
             begin
                  WriteLn('While entered: ' + IntToStr(whileCounter));
             end;

          pF := computePreferenceF();
          pS := computePreferenceS();
          M := sumMatrix(pF,pS);
          g := findTwo();

          if (g[1] = 999999) and (g[2] = 999999) then
             begin
                  WriteLn('No 2 found');
                  Finished:= true;
             end else
            //A 2 in M is found
            if canTransferToFarm() = false then
               if canTransferToShop() = false then
                  //No transfers are found, we need to swap
                  begin
                       swappedShop:= g[2];
                       v := findFarmForShop(g[2]);
                       currentShop[v] := currentShop[g[1]];

                       currentProfitFarm[v] := F[v,currentShop[v]];
                       currentProfitShop[v] := S[v,currentShop[v]];
                       transferF[v] := 0;
                       transferS[v] := 0;

                       currentShop[g[1]] := swappedShop;
                       currentProfitFarm[g[1]] := F[g[1],currentShop[g[1]]];
                       currentProfitShop[g[1]] := S[g[1],currentShop[g[1]]];
                       transferF[g[1]] := 0;
                       transferS[g[1]] := 0;

                  end;
     end;
     WriteLn('----------------------------- doSearch Finished -----------------------------');
end;

//Writes the solution to txt files
procedure writeOutput();
var shopFile,tFFile,tSFile, profitSFile, profitFFile : TextFile;
    i: Integer;
begin
     AssignFile(shopFile,'shopFile.txt');
     AssignFile(tFFile,'tFFile.txt');
     AssignFile(tSFile,'tSFile.txt');
     AssignFile(profitSFile,'profitSFile.txt');
     AssignFile(profitFFile,'profitFFile.txt');

     Rewrite(shopFile);
     Rewrite(tFFile);
     Rewrite(tSFile);
     Rewrite(profitFFile);
     Rewrite(profitSFile);

     for i := 1 to 120 do
         begin
              Write(shopFile, ' ' + IntToStr(currentShop[i]));
              Write(profitSFile, ' ' + FloatToStr(currentProfitShop[i]));
              Write(profitFFile, ' ' + FloatToStr(currentProfitFarm[i]));
              Write(tFFile, ' ' + FloatToStr(transferF[i]));
              Write(tSFile, ' ' + FloatToStr(transferS[i]));
         end;


     CloseFile(shopFile);
     CloseFile(tFFile);
     CloseFile(tSFile);
     CloseFile(profitFFile);
     CloseFile(profitSFile);

end;

begin
  r := 0.6;
  //Load Matrices
  initMatrix();

  initArrays();

  doSearch();

  //Write to files
  writeOutput();

  WriteLn('--------------------------- Execution finished ---------------------------');

  WriteLn(FloatToStr(calculateProfit()));
  ReadLn();
end.

